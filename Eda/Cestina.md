21.11.2018

#  Měšťanská literatura

- vedle duchovní, rytířské, dvorské...
- tyto motivy padají do mešťanského prostředí
- z měst se stává silný subjekt, který se časem stane hlavním
- města mají své univerzity (žáci umějí psát, mluvit cizími jazyky), dále kláštery
-> přibývá lidí, kteří umějí číst a psát
- přibývá kupců, podnikatelů - mají menší smysl pro "duchovno", mají lepší vnímání pro reálný svět, chtějí se bavit
- měšťanská literatura se vyznačuje realismem, satirou, výsměchem

## Měšťanská epika

 - patří do ní **fably** (= krátké epické veršované povídky sloužící k pobavení)
