19.11.2018

# Opěrná a pohybová soustava:

## Hydrostatická kostra
- tělo drží pokožka (např. žížala)
  - vnější kostra (exoskelet)
  - blokuje růst; tvořena chitinem, zbytek proteiny
  - u měkkýšů skelet stále roste, přidává se na stávající vrstvy
  - u arthropod je nutné svlékání
- vnitřní kostra (endoskelet)
  -výztuha zevnitř; chrupavky, kosti
  - především obratlovci, ale i bezobratlí (ochrana mozku chobotnice, sépiová kost, korálnatci)
  - paryby mají chrupavčitou kostru
- Typy pohybu
pomocí svalů, bičíků, brv, indulující membrány



21.11.2018

# Svaly

- je složen ze svalových vláken
  - tzv. myofibrily (= vlákno, skládající se z bílkovin)
- bílkoviny aktin a myozin (= aktinomyozinový komplex)
