# Analýza tvaru (rubriky)[EDA/MIKUL]

# Filosofie [EDA]

# Stylovost 3 rubrik [MIKUL]

## Velký rozhovor s jednou osobností

Většinou je osobnost i na titulní straně. Při pohledu na celý časopis je tento rozhovor hlavním prvkem.
Sleduje jak známé osobnosti, tak i osobu se silným / zajímavým příběhem.
Ve valné většině pojmuto formou velkého rozhovoru s otevřenými otázkami.
Otázky neočekávají jasnou odpověd ANO/NE.
Osobnosti je věnováno i 7 stránek, z čehož 2 bývají pouze fotografie spojené s osobností.
Čtenář je na počátku uveden do "děje", poté otázky směřují hlavně k aktuálním událostem spojeným s osobou.
Občas se přidá i malý sloupek, který popisuje danou osobnost další postavou, například jeho kolegou.
Jazykem se přizpůsobí dané osobnosti. S teenagerem tedy budou mluvit volněji, s politikem vážněji.


## Historické příběhy / fakta / události

Některé stránky se věnují událostem z minulosti.
Nebývají psány formou rozhovoru, spíše je to popis/vyprávění.
Většinou tam účinkuje nějaká postava z dnešní doby.
Může to tedy být aktuální příběh o tom, jak někdo přišel na zajímavá fakta z minulosti.
Může tak popisovat / připomínat skoro každou historickou událost.
Volba témata je zde už výrazně volnější.
Reportér si může vybrat, v době psaní článku, hodně diskutovnané téma, nebo může "vytáhnout" už zapomenutá fakta.
Stylově je to tedy souvislý vyprávěný text. Styl (spisovnost/volnost) závisí na reportérovi.
Bývá vždy tak na 2-4 stránky. Může být přiložena fotka, pokud je to vhodné


# Hledat známé novináře (pravidelné)[EDA]


Výrazná osobnost - Jaroslav Kmenta
Jaroslav Kmenta patří ke špičce české investigativní novinařiny. Zaměřuje se na dění v české politické scéně spojené s korupcí a propojení s podnikatelskými a mafiánskými kruhy. Podílel se na rozkývání takových kauz, jako například kauza kmotra Mrázka (doplnit), zločineckého podnikatele Radovana Krejčího či podvodům současného premiéra Andreje Babiše. Na podkladech jeho práce byly natočeny i dva filmy, Příběh Kmotra a Gangster Ka. (přidám)


# Design časopisu [MIKUL]
