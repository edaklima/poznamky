# Fredyho hlášky
- Elektra o hodině celkem kecá, Fredy si toho všime
	> Elektro, vy jste nějak elektrizovaná, ne? ... Tyjo to se mi povedlo. Vy jste mě opravdu asi nabila.
- Fredy mluví o strašidelných barokních pohádkách
	> Juj, to se budem bát ve stanu!
- bez kontextu
	> Baroko je takové výbušné pššš...

---

24.10.2018

# Středověká literatura

- Většinou psáno latinsky
- Dokonce i scripta z medicíny se psaly ve verších (asi aby se to lépe učilo)

## Scholastika

- = Bernard z Clairvaux (1153+)
	- reformátor
	- zastánce realismu -> realismu, ?kde všechny věci doopravdy existují?
- diskuze, kde rozumem se mělo potvrdit, že to tak doopravdy je
- dokonce stanovili oponenta ( pro diskuze )
- => písemné přijímání dogmat

## Nominalismus

- = Abélard
	- -> Historie mých pohrom
	- teolog v Paříži, jeden z prvních nominalistů
 	- Nominalismus => pojmy existují pouze podle jména, ale rozum k nim musí blíž
		- "neexistuje lidstvo, existují lidé"
		- "rostlinstvo nezkoumám, zkoumám až tu rostlinu"
	- nejsou kacíři, jen věří, že rozum je síla a je potřeba ji využít

## Středověká vysoká škola

- Vznikají blízko kláštěrů, 4 fakutly
	- artisická (od 14 let)
	- medicína / práva (od 20 let)
	- teologie (od 35 let)
 - = John Scott
	- Františkán, mistr Oxfordu
	- zdůrazňoval roli vůle a možnost volby
 - = Vilém Occam
	- teorie jednotlivin => "není lidstvo, jsou lidé"
	- také z Oxfordu

### Žákovská produkce

- satira ve formě písniček, studenti si tak dělali z každého srandu
- studenti psali "nižší" literaturu, profesoři psali "vyšší"
-  = Primas
-  = Hugo Orelánský

---

31.10.2018

### Duchovní lyrika

- pana Marie, duchovní legendy, prostor pro lásku
- františkáni
	- = sv. Boneventura
		- povolil, že řád může mít majetek
	- = František z Assisi (12-13.stol)
		- hlavní osobností františkánů
		- snažili se reformovat církev, neútočili však na ni jako Hus
		- -> píseň na bratra Slunce (Slunce ve smyslu boha)
		- měl kamarádku


- = sv. Klára
	- zakladatelka klaristek (ženský klášter)
	- je též z Assisi

- dominikáni (1215)
	- Dominigo de Guzmán = zakladatel dominikánů
	- byla jim svěřena inkvizice
- kataři
	- sekta z jižní francie
	- křížová výprava proti katarům
- valdenští
	- = P. Valdo
	- dobrovolná chudoba, pokora,mír

---

1.11.2018

## Duchovní literatura
- mnoho žánrů souvisejících s vírou

---

2.11.2018

## Dvorská epika
- nejdříve písně o hrdinských činech
- -> courtoázní kultura
	- => dvorní kultura, odehrává se na dvorech - střední + vyšší společnost
	- rytíř, který je vzorem, chová se velice zdvořile k ženám
		- neustále je se ženami v kontaktu, dvoří se i vdaným ženám
		- za ženy i bojovali
### Rytířský román
- delí se na několik okruhů:

	- **antický**
		- zpracování antických příběhů = příběhy Alexandra velikého (Alexandreidy)
		= Dějiny dobití Tróje
		= Román o Thébách
	- **bretonský / keltský**
		- oblast severozápadní Francie, ústup před sasy, usazují se tam Britoni
		= Král Artuš a jeho rytíři u kulatého stolu
		= Mýtus o sv. Grálu
---

9.11.2018

## Dvorská lyrika/poezie
- Vrchol kulturního zážitku
- Téma o **lásce** v jakékoli podobě
- Původně byla *erotika* hodně RAW, postupně se začala anonymizovat
- Alba -> svítáníčko
	- loučení páru po společně strávené noci
